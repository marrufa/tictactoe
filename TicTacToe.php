<?php

$folders = ['helpers','exceptions','interfaces', 'abstract', 'mocks', 'models', 'services', 'controllers'];
foreach($folders as $folder){
	foreach (glob("src/".$folder."/*.php") as $files) {
	    include $files;
	}	
}


// maybe a framework can do dependency injection...
$factoryPlayer = new mocks\PlayersMock();
$servicePlayer = new services\PlayerService($factoryPlayer);
$controllerPlayer = new controllers\PlayerController($servicePlayer);

$factoryGame = new mocks\GamesMock();
$serviceGame = new services\GameService($factoryGame);
$controllerGame = new controllers\GameController($serviceGame);

$factoryMove = new mocks\MovesMock();
$serviceMove = new services\MoveService($factoryMove);
$controllerMove = new controllers\MoveController($serviceMove, $serviceGame);


// creating two players
$player1 = $controllerPlayer->createPlayer('{"name":"marc"}');
$player2 = $controllerPlayer->createPlayer('{"name":"fabegat"}');

// starting game
$game = $controllerGame->createGame('{"player1":{"id":"'.$player1->getId().'"}, "player2":{"id":"'.$player2->getId().'"}}');
$game = $controllerGame->startGame('{"id":"'.$game->getId().'"}');
//$game = $controllerGame->infoGame('{"id":"'.$game->getId().'"}');


/// Start automatic Game!
/*
$player1move = $controllerMove->makeMove(
	'{"gameId":"'.$game->getId().'", "playerId":"'.$player2->getId().
	'", "row":"'.rand(1,3).'", "col":"'.rand(1,3).'"}');
*/

/*
* define in MoveService
*  dashboard
......1.....2.....3
1....11....12....13 
2....21....22....23 
3....31....32....33 

win operations
sum(11,12,13) = 36
sum(21,22,23) = 66
sum(31,32,33) = 96
sum(11,21,31) = 63
sum(12,22,32) = 66
sum(13,23,33) = 69
sum(11,22,33) = 66
sum(13,22,31) = 66
*/

try{
	$player1move = $controllerMove->makeMove('{"gameId":"'.$game->getId().'", "playerId":"'.$player1->getId().
		'", "value":"22"}');  //center
	echo "Move by player ".$player1->getId()." -> 22\n";

	$player2move = $controllerMove->makeMove('{"gameId":"'.$game->getId().'", "playerId":"'.$player2->getId().
		'", "value":"31"}');  // left bottom
	echo "Move by player ".$game->getId()." -> 31\n";

	$player1move = $controllerMove->makeMove('{"gameId":"'.$game->getId().'", "playerId":"'.$player1->getId().
		'", "value":"32"}');  // center bottom
	echo "Move by player ".$player1->getId()." -> 32\n";

	$player2move = $controllerMove->makeMove('{"gameId":"'.$game->getId().'", "playerId":"'.$player2->getId().
		'", "value":"21"}');  // center left
	echo "Move by player ".$game->getId()." -> 21\n";


	$player1move = $controllerMove->makeMove('{"gameId":"'.$game->getId().'", "playerId":"'.$player1->getId().
		'", "value":"12"}');  // center top
	echo "Move by player ".$player1->getId()." -> 12\n";

	$game = $controllerGame->infoGame('{"id":"'.$game->getId().'"}');
	if($game->getStatus() == models\GameStatus::WINNER){
		echo "Game win by player ".$game->getWinnerId()."\n\n";

	}

} catch(exceptions\InvalidMoveException $e){
	helpers\Logger::error($e->getMessage());
}


$controllerPlayer->deletePlayer('{"id":"'.$player1->getId().'"}');
$controllerPlayer->deletePlayer('{"id":"'.$player2->getId().'"}');

