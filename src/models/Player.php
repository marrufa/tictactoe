<?php

namespace models;

use models\BaseModel;

class Player extends BaseModel {

	protected $name;

	function __construct($name = null){
		$this->name = $name;
	}

	public function getName() {
		return $this->name;
	}

}
