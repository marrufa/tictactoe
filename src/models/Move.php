<?php

namespace models;

use models\BaseModel;


class Move extends BaseModel {

	protected $gameId;
	protected $playerId;
	protected $value;

	function __construct($gameId, $playerId, $value){
		$this->gameId = $gameId;
		$this->playerId = $playerId;
		$this->value = $value;
	}

	public function getGameId(){
		return $this->gameId;
	}
	public function setGameId($gameId){
		$this->gameId = $gameId;
	}

	public function getPlayerId(){
		return $this->playerId;
	}
	public function setPlayerId($playerId){
		$this->playerId = $playerId;
	}

	public function getValue(){
		return $this->value;
	}
	public function setValue($value){
		$this->value = $value;
	}

}