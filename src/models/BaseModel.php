<?php

namespace models;

abstract class BaseModel {
	
	protected $id;

	// this method is crazy, the id should by created by external service or bdd
	public function setId($id){
		$this->id = $id;
	}
	public function getId(){
		return $this->id;
	}

}
