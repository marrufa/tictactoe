<?php

namespace models;

abstract class MoveStatus {

	const GOOD = "good";  // in the place
	const BUSY = "busy";  // busy position
	const FAIL = "fail";  // outofbound
	const WIN  = "winner"; // winner move!

}
