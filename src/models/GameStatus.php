<?php

namespace models;

abstract class GameStatus {

	const INIT = "new";
	const STARTED = "started";
	const FINISHED = "finished";
	const WINNER = "winner";
}