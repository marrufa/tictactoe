<?php

namespace models;

use interfaces\iGame;
use models\BaseModel;
use models\GameStatus;

class Game extends BaseModel implements iGame {

	protected $playersId;
	protected $status;
	protected $winnerId;

	// lets start with hardcoded two players... but not limit data structure to two
	function __construct($playerId1, Player $playerId2){
		$this->playersId[0] = $playerId1;
		$this->playersId[1] = $playerId2;
		$this->status = GameStatus::INIT;
	}

	public function getPlayers() {
		return $this->playersId;
	}

	public function getStatus(){
		return $this->status;
	}
	public function setStatus($status){
		$this->status = $status;
	}

	public function getWinnerId(){
		return $this->winnerId;
	}
	public function setWinnerId($playerId){
		$this->winnerId = $playerId;
	}

	public function isFinished(){
		return ($this->getStatus() == GameStatus::FINISHED);
	}

}