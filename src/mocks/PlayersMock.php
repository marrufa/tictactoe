<?php

namespace mocks;

use mocks\BaseMock;
use models\Player;

class PlayersMock extends BaseMock {

	function __construct(){
		$this->models = new \ArrayObject([ 
			new Player("99", "marc"),
			new Player("98", "fabregat"),
		]);
	}

}
