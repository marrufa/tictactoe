<?php

namespace mocks;

use mocks\BaseMock;
use models\Player;
use models\Game;

class GamesMock extends BaseMock {
	
	function __construct(){
		$this->models = new \ArrayObject([ 
			new Game(new Player("marc"), new Player("fabregat")),
		]);
	}

}