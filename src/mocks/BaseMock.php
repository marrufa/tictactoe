<?php

namespace mocks;

use interfaces\iStorage;
use helpers\Logger;

class BaseMock implements iStorage {
	
	protected $models;

	public function readAll($id) {
		$retValue = array();
		if($id!=null){
			foreach($this->models as $model){
				if ( $model->getId() == $id){
					array_push($retValue, $model);
				}
			}
			return $retValue;
		} else {
			return $this->models;
		}
		return null;
	}

	public function read($id) {
		foreach($this->models as $model){
			if ( $model->getId() == $id){
				return $model;
			}
		}
		return null;
	}

	public function save($model) {
		if($model->getId()==null){
			$model->setId(uniqid());	
		}
		$this->models->append($model);
		Logger::info("model [".$model->getId()."] - ".get_class($this)." saved");
		return $model;
	}

	public function delete($id){
		$deleteKey = null;
		foreach($this->models as $key=>$value){
			if ( $value->getId() == $id){
				$deleteKey = $key;
				break;
			}
		}
		if($deleteKey!=null){
			$this->models->offsetUnset($deleteKey);
			Logger::info("model [".$id."] - ".get_class($this)." deleted");
		}
	}
}