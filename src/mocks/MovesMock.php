<?php

namespace mocks;

use mocks\BaseMock;
use models\Move;

class MovesMock extends BaseMock {

	function __construct(){
		$this->models = new \ArrayObject([ 
			new Move("99", "98", "11"),
		]);
	}

	public function readAllByGame($id) {
		$retValue = array();
		if($id!=null){
			foreach($this->models as $model){
				if ( $model->getGameId() == $id){
					array_push($retValue, $model);
				}
			}
			return $retValue;
		} else {
			return $this->models;
		}
		return null;
	}
}