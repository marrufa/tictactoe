<?php

namespace interfaces;

use models\BaseModel;

interface iService {

	public function read($id);

	public function save(BaseModel $model);

	public function delete($id);


}
