<?php

namespace interfaces;

interface iStorage {
	
	public function read($id);

	public function readAll($id);

	public function save($model);

	public function delete($model);

}
