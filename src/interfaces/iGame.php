<?php

namespace interfaces;

interface iGame {
	
	public function isFinished();

	public function getWinnerId();
	public function setWinnerId($winnerId);
	
	public function getStatus();
	public function setStatus($status);
	
}