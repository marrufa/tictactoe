<?php

namespace helpers;

class Logger {
	
	public static function info($msg){
		echo "INFO - ".$msg."\n";
	}

	public static function error($msg){
		echo "ERROR - ".$msg."\n";
	}

}
