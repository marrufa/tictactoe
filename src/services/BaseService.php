<?php

namespace services;

use interfaces\iService;
use interfaces\iStorage;
use models\BaseModel;

abstract class BaseService implements iService {
	
	protected $storage;

	function __construct(iStorage $storage){
		$this->storage = $storage;
	}

	public function read($id) {
		return $this->storage->read($id);
	}

	public function save(BaseModel $model) {
		return $this->storage->save($model);
	}

	public function delete($id){
		$this->storage->delete($id);
	}

}
