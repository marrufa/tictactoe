<?php

namespace services;

use services\BaseService;
use models\Player;

class PlayerService extends BaseService {
	
	// probably models returned by service must be diferent (smaller than real bdd)
	public function createPlayer(Player $player){
		return $this->save($player);	
	}

	public function deletePlayer($playerId){
		$this->delete($playerId);
	}

}
