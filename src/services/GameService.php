<?php

namespace services;

use services\BaseService;
use interfaces\iGame;
use models\GameStatus;

// probably models returned by service must be diferent (smaller than real bdd)
class GameService extends BaseService {

	function createGame(iGame $game){
		return $this->save($game);	
	}

	function startGame($gameId){
		$game = $this->read($gameId);
		$game->setStatus(GameStatus::STARTED);
		return $this->save($game);
	}

	function setWinner($gameId, $playerId){
		$game = $this->read($gameId);
		$game->setWinnerId($playerId);
		$game->setStatus(GameStatus::WINNER);
		return $this->save($game);
	}

	function setFinished($gameId){
		$game = $this->read($gameId);
		$game->setStatus(GameStatus::FINISED);
		return $this->save($game);
	}

	function infoGame($gameId){
		return $this->read($gameId);
	}

	function deleteGame($gameId){
		$this->delete($gameId);
	}
}