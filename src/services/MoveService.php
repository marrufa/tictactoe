<?php

namespace services;

use services\BaseService;
use models\Move;
use models\MoveStatus;
use exceptions\InvalidMoveException;


class MoveService extends BaseService {

	// this can be inyected by MoveType method interfaced
	const ALL_MOVES = array(11,12,13, 21,22,23, 31,32,33);
	const WIN_VALUES = array(36, 63, 66, 69, 96);
	/*
	* define in MoveService
	*  dashboard
	......1.....2.....3
	1....11....12....13 
	2....21....22....23 
	3....31....32....33 

	win operations
	sum(11,12,13) = 36
	sum(21,22,23) = 66
	sum(31,32,33) = 96
	sum(11,21,31) = 63
	sum(12,22,32) = 66
	sum(13,23,33) = 69
	sum(11,22,33) = 66
	sum(13,22,31) = 66
	*/

	
	public function makeMove(Move $move){

		$status = MoveStatus::GOOD;
		// checl move limitatons
		$status = $this->checkValidMove($move);

		if($status != MoveStatus::FAIL){
			// check posible move in game!
			$moves = $this->storage->readAllByGame($move->getGameId());
			$status = $this->isFreePosition($moves, $move);

			if($status != MoveStatus::BUSY){
				$this->save($move);
				
				// check if 3 in a row
				$moves = $this->storage->readAllByGame($move->getGameId());
				$status = $this->checkWinerMoves($moves, $move->getPlayerId());

				if($status == MoveStatus::WIN){
					///mmmmm I gona to send back to controller the winner status
				}
			}
		}

		return $status;
	}

	private function isFreePosition($moves, $newMove){
		foreach($moves as $move){
			if( $move->getValue() == $newMove->getValue() ){
				return MoveStatus::BUSY;
			}
		}
		return MoveStatus::GOOD;
	}


	// more restrictions? by user?
	private function checkValidMove(Move $move){
		if( !in_array($move->getValue(), $this::ALL_MOVES) ){
			//retun MoveStatus::FAIL;
			throw new InvalidMoveException('Something wrong with frontend dashboard!');
		}
		return MoveStatus::GOOD;
	}

	public function checkWinerMoves($moves, $playerId){

		$sumMoves = 0;
		foreach($moves as $move){
			if($move->getPlayerId() == $playerId){
				$sumMoves = $sumMoves + $move->getValue();
				if( in_array($sumMoves, $this::WIN_VALUES)){
					return MoveStatus::WIN;
				}
			}
		}
		return MoveStatus::GOOD;
	}
}