<?php

namespace controllers;

use interfaces\iService;

class BaseController {
	
	protected $service;

	function __construct(iService $service){
		$this->service = $service;
	}
}
