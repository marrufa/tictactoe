<?php

namespace controllers;

use models\Move;
use models\MoveStatus;
use helpers\Logger;
use interfaces\iService;


class MoveController extends BaseController {
	
	protected $gameService;

	function __construct(iService $service, iService $gameService){
		parent::__construct($service);
		$this->gameService = $gameService;
	}

	public function makeMove($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->gameId) && isset($decoded->playerId)
			&& isset($decoded->value)){
			$move = new Move( 
				$decoded->gameId,
				$decoded->playerId,
				$decoded->value
			);
			$result = $this->service->makeMove($move);
			
			if( $result  == MoveStatus::WIN ){
				$this->gameService->setWinner($move->getGameId(), $move->getPlayerId());
			}
			return $result;
		} else {
			Logger::error("makeMove - fail with request[".$request."]");
		}
	}

}