<?php

namespace controllers;

use models\Player;
use helpers\Logger;

class PlayerController extends BaseController {
	

	public function createPlayer($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->name)){
			$player = new Player( 
				$decoded->name
			);
			return $this->service->save($player);	
		} else {
			Logger::error("createPlayer - fail with request[".$request."]");
		}
	}

	public function deletePlayer($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->id)){

			// we should check if player have active games... what we do in case?
			$this->service->delete($decoded->id);
		} else {
			Logger::error("deletePlayer - fail with request[".$request."]");
		}
	}
}