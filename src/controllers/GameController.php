<?php

namespace controllers;

use models\Player;
use models\Game;
use helpers\Logger;


class GameController extends BaseController {
	
	public function createGame($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->player1) && isset($decoded->player2)){
			$game = new Game(
				new Player($decoded->player1->id),
				new Player($decoded->player2->id)
			);
			return $this->service->save($game);	
		} else {
			Logger::error("createGame - fail with request[".$request."]");
		}
	}

	public function startGame($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->id)){
			return $this->service->startGame($decoded->id);
		} else {
			Logger::error("startGame - fail with request[".$request."]");
		}
	}


	public function infoGame($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->id)){
			return $this->service->infoGame($decoded->id);
		} else {
			Logger::error("infoGame - fail with request[".$request."]");
		}
	}

	public function deleteGame($request){
		$decoded = json_decode($request);
		
		// framework must have responsability of check json integrity check
		if($decoded!=null && isset($decoded->id)){
			$this->service->delete($decoded->id);
		} else {
			Logger::error("deleteGame - fail with request[".$request."]");
		}
	}
}